import { Component, OnInit } from '@angular/core';
import {Employee} from '../employee';
import {EmployeeService} from '../employee.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employee: Employee;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
  }

  add(fullName: String, address: String, email: String, phoneNumber: String): void {
    this.employeeService.addEmployee({ fullName, address, email, phoneNumber } as Employee);
  }

  // onSubmit(): void {
  //   this.employeeService.addEmployee(this.employee);
  // }

}
