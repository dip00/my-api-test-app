import { Component, OnInit } from '@angular/core';
import {Employee} from '../employee';
import {EmployeeService} from '../employee.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];

  constructor(private employeeService: EmployeeService) {
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
  }

  ngOnInit() {
    this.getEmployees();
  }

  deleteEmployee(employee: Employee) {
    console.log(1);
    this.employees = this.employees.filter(e => e !== employee);
    // why?
    this.employeeService.deleteEmployee(employee).subscribe();
  }
}
