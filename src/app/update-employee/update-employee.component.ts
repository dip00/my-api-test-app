import {Component, Input, OnInit} from '@angular/core';
import {Employee} from '../employee';
import {ActivatedRoute} from '@angular/router';
import {EmployeeService} from '../employee.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  @Input() employee: Employee;

  constructor(private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private location: Location) { }

  ngOnInit() {
    this.getEmployee();
  }

  getEmployee(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.employeeService.getEmployee(id).subscribe(employee => this.employee = employee );
  }

  onSubmit() {
    this.employeeService.updateEmployee(this.employee.id, this.employee).
    subscribe(() => this.goBack());
  }
  goBack(): void {
    this.location.back();
  }

}
